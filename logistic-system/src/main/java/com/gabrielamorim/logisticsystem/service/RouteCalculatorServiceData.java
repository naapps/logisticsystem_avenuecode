package com.gabrielamorim.logisticsystem.service;

/**
 * 
 * @author Gabriel Amorim
 * 
 *         This class contains all informations required to calculate the
 *         shortest way and the lower delivery value.
 *
 */
public class RouteCalculatorServiceData {

	private String name;
	private String from;
	private String to;
	private int autonomy;
	private double consume;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public int getAutonomy() {
		return autonomy;
	}

	public void setAutonomy(int autonomy) {
		this.autonomy = autonomy;
	}

	public double getConsume() {
		return consume;
	}

	public void setConsume(double consume) {
		this.consume = consume;
	}
}
