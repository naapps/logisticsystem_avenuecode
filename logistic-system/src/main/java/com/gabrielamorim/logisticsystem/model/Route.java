package com.gabrielamorim.logisticsystem.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * 
 * @author Gabriel Amorim
 * 
 *         This class is an entity to persist the routes of maps. A route is
 *         analogous to a vertex since it contains a starting point
 *         {@link Route#fromNode}, an ending point {@link Route#toNode} and the
 *         {@link Route#distance} between these points.
 *
 */
@Entity
public class Route {

	@Id
	@GeneratedValue
	private Long id;

	private String fromNode;
	private String toNode;
	private int distance;

	public String getFromNode() {
		return fromNode;
	}

	public void setFromNode(String fromNode) {
		this.fromNode = fromNode;
	}

	public String getToNode() {
		return toNode;
	}

	public void setToNode(String toNode) {
		this.toNode = toNode;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
