package com.gabrielamorim.logisticsystem.dao;

import javax.persistence.EntityManager;

/**
 * 
 * @author Gabriel Amorim
 * 
 *         This GenericDAO encapsulates the transactions to others DAOs.
 *
 */
public class GenericDAO {
	
	public static final String persistenceUnit = "logistic-system";
	
	protected EntityManager em;	
	
	protected void beginTransaction() {
		em = JPAUtil.getEntityManager(persistenceUnit);
		em.getTransaction().begin();
	}
	
	protected void endTransaction() {
		try {
			em.getTransaction().commit();
		} catch(Exception e) {
			System.err.println("GenericDAO.endTransacation(): " + e.getMessage());	
		} finally {
			em.close();
		}
	}
}
