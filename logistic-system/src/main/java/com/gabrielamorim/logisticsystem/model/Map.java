package com.gabrielamorim.logisticsystem.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * 
 * @author Gabriel Amorim
 * 
 *         This class is an entity to maps into the logistic system. It contains
 *         the {@link Map#name} and the {@link Map#routes}.
 *
 */
@Entity
public class Map {

	@Id
	private String name;

	@OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	private List<Route> routes;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Route> getRoutes() {
		return routes;
	}

	public void setRoutes(List<Route> routes) {
		this.routes = routes;
	}
}
