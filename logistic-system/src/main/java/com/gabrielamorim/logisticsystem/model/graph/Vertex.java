package com.gabrielamorim.logisticsystem.model.graph;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Gabriel Amorim
 * 
 *         A vertex or node is the fundamental unit of which graphs are formed.
 *         It consists of its {@link Vertex#name}, {@link Vertex#dist} between
 *         the {@link Vertex#previous} Vertex and a map with the
 *         {@link Vertex#neighbours} of this Vertex.
 *
 */
public class Vertex implements Comparable<Vertex> {

	public final String name;
	public int dist = Integer.MAX_VALUE;
	public Vertex previous = null;
	public final Map<Vertex, Integer> neighbours = new HashMap<>();

	public Vertex(String name) {
		this.name = name;
	}

	/**
	 * This method adds the Vertex into the shortestPath list.
	 * 
	 * @param shortestPath
	 */
	public void addToPath(List<Vertex> shortestPath) {
		if (this == this.previous) {
			shortestPath.add(this);
		} else if (this.previous == null) {
			shortestPath.add(this);
		} else {
			this.previous.addToPath(shortestPath);
			shortestPath.add(this);
		}
	}

	public int compareTo(Vertex other) {
		return Integer.compare(dist, other.dist);
	}
}
