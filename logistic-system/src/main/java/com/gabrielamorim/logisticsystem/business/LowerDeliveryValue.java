package com.gabrielamorim.logisticsystem.business;

import java.util.List;

import com.gabrielamorim.logisticsystem.model.Map;
import com.gabrielamorim.logisticsystem.model.graph.Edge;
import com.gabrielamorim.logisticsystem.model.graph.Graph;
import com.gabrielamorim.logisticsystem.model.graph.Vertex;

/**
 * 
 * @author Gabriel Amorim
 * 
 *         This is a business class to evaluate the lower delivery value.
 *
 */
public class LowerDeliveryValue {

	public String process(Map map, String from, String to, int autonomy,
			double consume) {

		Edge[] graph = convertMapToGraphArray(map);
		
		Graph g = new Graph(graph);
		RouteCalculator rc = new RouteCalculator(g);
		rc.computeShortestPath(from);
		List<Vertex> shortesPath = rc.getShortestPath(to);
		
		double distance = shortesPath.get(shortesPath.size() - 1).dist;
		String route = "";
		
		for(Vertex vertex : shortesPath) {
			route = route + vertex.name + " ";
		}
		
		double cost = (distance / autonomy) * consume;
		
		return "Route " + route + " - cost " + cost;
	}

	private Edge[] convertMapToGraphArray(Map map) {
		
		Edge[] graph = new Edge[map.getRoutes().size()];
	
		for(int i = 0; i <  map.getRoutes().size(); i++) {
			graph[i] = new Edge(map.getRoutes().get(i).getFromNode(), map.getRoutes().get(i).getToNode(), map.getRoutes().get(i).getDistance());
		}
		
		return graph;
	}
}
