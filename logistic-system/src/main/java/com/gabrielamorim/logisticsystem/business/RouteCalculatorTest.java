package com.gabrielamorim.logisticsystem.business;

import java.util.List;

import com.gabrielamorim.logisticsystem.model.graph.Edge;
import com.gabrielamorim.logisticsystem.model.graph.Graph;
import com.gabrielamorim.logisticsystem.model.graph.Vertex;


public class RouteCalculatorTest {

	private static final Edge[] GRAPH = {
	      new Edge("A", "B", 10),
	      new Edge("B", "D", 15),
	      new Edge("A", "C", 20),
	      new Edge("C", "D", 30),
	      new Edge("B", "E", 50),
	      new Edge("D", "E", 30)
	   };
	   
	private static final String START = "A";
	private static final String END = "D";
	   
	public static void main(String[] args) {
		Graph g = new Graph(GRAPH);
		RouteCalculator rc = new RouteCalculator(g);
		rc.computeShortestPath(START);
		List<Vertex> shortesPath = rc.getShortestPath(END);		
		
		for(Vertex v : shortesPath) {
			System.out.printf("%s (%d) -> ", v.name, v.dist);
		}
	}
}