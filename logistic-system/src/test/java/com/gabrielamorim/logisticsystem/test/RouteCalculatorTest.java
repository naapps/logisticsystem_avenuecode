package com.gabrielamorim.logisticsystem.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.gabrielamorim.logisticsystem.business.RouteCalculator;
import com.gabrielamorim.logisticsystem.model.graph.Edge;
import com.gabrielamorim.logisticsystem.model.graph.Graph;

public class RouteCalculatorTest {
	
	@Test
	public void testExcute() {
		
		final Edge[] GRAPH = {
			      new Edge("A", "B", 10),
			      new Edge("B", "D", 15),
			      new Edge("A", "C", 20),
			      new Edge("C", "D", 30),
			      new Edge("B", "E", 50),
			      new Edge("D", "E", 30)
			   };

		Graph g = new Graph(GRAPH);
		RouteCalculator rc = new RouteCalculator(g);
		rc.computeShortestPath("A");
		rc.getShortestPath("D");	
		
		
		assertNotNull(g);
//		assertTrue(path.size() > 0);

		

	}
}
