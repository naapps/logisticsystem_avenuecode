package com.gabrielamorim.logisticsystem.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.gabrielamorim.logisticsystem.dao.MapDAO;
import com.gabrielamorim.logisticsystem.model.Map;

/**
 * 
 * @author Gabriel Amorim
 * 
 *         This class exposes the API to the Maps Manager Service, which permits
 *         that clients save maps into the logistic system. It's implemented
 *         with JAX-RS.
 *
 */
@Path("/map-manager")
public class MapsManagerService {

	@POST
	@Path("/post")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createMap(Map map) {

		MapDAO mapDao = new MapDAO();
		mapDao.save(map);

		return Response.status(Response.Status.CREATED).build();

	}

}
