package com.gabrielamorim.logisticsystem.model.graph;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Gabriel Amorim
 * 
 *         This class represents a graph. It receives a series of edges to
 *         construct a graph using a map of vertices {@link Graph#graph}.
 *
 */
public class Graph {

	private final Map<String, Vertex> graph;

	public Graph(Edge[] edges) {
		graph = new HashMap<>(edges.length);

		//find all vertices
		for (Edge e : edges) {
			if (!graph.containsKey(e.v1))
				graph.put(e.v1, new Vertex(e.v1));
			if (!graph.containsKey(e.v2))
				graph.put(e.v2, new Vertex(e.v2));
		}

		//find neighbouring vertices
		for (Edge e : edges) {
			graph.get(e.v1).neighbours.put(graph.get(e.v2), e.dist);
		}
	}

	public Map<String, Vertex> getEdges() {
		return graph;
	}
}
