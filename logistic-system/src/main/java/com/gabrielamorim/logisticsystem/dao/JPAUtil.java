package com.gabrielamorim.logisticsystem.dao;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 * 
 * @author Gabriel Amorim
 * 
 *         This JPAUtil avoids the creation of EntitiesManagersFactories all the
 *         time that an EntityManager is required. All EntityManagerFactory is
 *         stored in a static Map.
 *
 */
public class JPAUtil {

	private static Map<String, EntityManagerFactory> emfHash = new HashMap<String, EntityManagerFactory>();

	public static EntityManager getEntityManager(String source) {
		if (emfHash.get(source) == null) {
			EntityManagerFactory emf = Persistence
					.createEntityManagerFactory(source);
			emfHash.put(source, emf);
			return emf.createEntityManager();
		} else {
			return emfHash.get(source).createEntityManager();
		}
	}
}
