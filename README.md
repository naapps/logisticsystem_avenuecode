# README #


#Installing the application

- Create a DB Schema named logistic_system on MySQL localhost.
- Set DB configurations on persistence.xml from the application (This application is using the hibernate.hbm2ddl.auto update, so it is not necessary to run a DDL to create tables. When the application starts the database will be created.)
- Run mvn package and deploy the WAR file generated into a Web Container such as Tomcat.
- Start the Web Container and all right, the app must be up and running!

#Using the application

- To create a new map, send a JSON request like below to the Map Manager Service exposed in the following address (http://SERVER_ADDRESS/logistic-system/map-manager/post). This service consumes a JSON representation of a map and persists the map on DB.

{ 
	"name" : "mapa1", 
	"routes" : 
		[ 
			{ "fromNode" : "A", "toNode" : "B", "distance" : 10 }, 
			{ "fromNode" : "B", "toNode" : "D", "distance" : 15 }, 
			{ "fromNode" : "A", "toNode" : "C", "distance" : 20 },
			{ "fromNode" : "C", "toNode" : "D", "distance" : 30 },
			{ "fromNode" : "B", "toNode" : "E", "distance" : 50 },
			{ "fromNode" : "D", "toNode" : "E", "distance" : 30 }
		] 
}

- To find the lower delivery cost, send a JSON request (a) like below to the Route Calculator Service exposed in the following address (http://SERVER_ADDRESS/logistic-system/route-calculator/post). This service consumes the parameters to calculate and return the lower delivery cost like below JSON response (b). 

(a)
{
	"name" : "mapa1",
	"from" : "A",
	"to" : "D",
	"autonomy" : 10,
	"consume" : 2.50
}

(b)
{
    "result": "Route A B D  - cost 6.25"
}

#Architecture

- This application exposes services using JAX-RS. JAX-RS is an API to create REST web services. This choice was motivated by the simplicity and efficiency of the REST pattern to integration between different systems, promoting among other things, loose coupling and an uniform interface. With REST, we are free of things commons to the traditional SOA approach, like  WSDL, SOAP, XML and so on, so improving the maintenance of the system. Besides, this approach facilitates the scalability and performance improvements in an statelessness environment. Read more in http://blog.gabrielamorim.com/rest-jax-rs-resteasy-integracao/

- This application also adopted JSON as the format lightweight to data interchange between systems. JSON is simple to write, read and convert by humans and machines and is language independent.

