package com.gabrielamorim.logisticsystem.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.gabrielamorim.logisticsystem.business.LowerDeliveryValue;
import com.gabrielamorim.logisticsystem.dao.MapDAO;
import com.gabrielamorim.logisticsystem.model.Map;

/**
 * 
 * @author Gabriel Amorim
 * 
 *         This class exposes the API to the Route Calculator Service, which permits
 *         that clients calculates the lower delivery value. It's implemented
 *         with JAX-RS.
 *
 */
@Path("/route-calculator")
public class RouteCalculatorService {
	
	@POST
	@Path("/post")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response lowerDeliveryValue(RouteCalculatorServiceData data) {
		MapDAO mapDao = new MapDAO();
		Map map = mapDao.getMap(data.getName());
		
		if(map.getName().isEmpty()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		String result = new LowerDeliveryValue().process(map, data.getFrom(), data.getTo(), data.getAutonomy(), data.getConsume());
		
		return Response.ok("{ \"result\" : \"" + result + "\" }", MediaType.APPLICATION_JSON).build();		
	}
}
