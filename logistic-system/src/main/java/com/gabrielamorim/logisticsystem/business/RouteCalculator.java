package com.gabrielamorim.logisticsystem.business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.TreeSet;

import com.gabrielamorim.logisticsystem.model.graph.Graph;
import com.gabrielamorim.logisticsystem.model.graph.Vertex;

/**
 * 
 * @author Gabriel Amorim
 * 
 *         This class is responsible to provide an implementation of the
 *         Dijkstra algorithm to find the shortest path in a graph. This
 *         algorithm is used to determine the best way in a map to the logistic
 *         system.
 *
 */
public class RouteCalculator {

	private Graph graph;

	public RouteCalculator(Graph graph) {
		this.graph = graph;
	}

	/**
	 * Runs dijkstra using a specified source vertex.
	 * */
	public void computeShortestPath(String startName) {
		if (!graph.getEdges().containsKey(startName)) {
			System.err.printf("Graph doesn't contain start vertex \"%s\"\n",
					startName);
			return;
		}
		final Vertex source = graph.getEdges().get(startName);
		NavigableSet<Vertex> q = new TreeSet<>();

		// set-up vertices
		for (Vertex v : graph.getEdges().values()) {
			v.previous = v == source ? source : null;
			v.dist = v == source ? 0 : Integer.MAX_VALUE;
			q.add(v);
		}

		computeShortestPath(q);
	}

	/**
	 * Implementation of dijkstra's algorithm using a binary heap.
	 * */
	private void computeShortestPath(final NavigableSet<Vertex> q) {
		Vertex u, v;
		while (!q.isEmpty()) {
			u = q.pollFirst();
			if (u.dist == Integer.MAX_VALUE)
				break; // we can ignore u (and any other remaining vertices)
						// since they are unreachable

			// look at distances to each neighbour
			for (Map.Entry<Vertex, Integer> a : u.neighbours.entrySet()) {
				v = a.getKey(); // the neighbour in this iteration

				final int alternateDist = u.dist + a.getValue();
				if (alternateDist < v.dist) { // shorter path to neighbour found
					q.remove(v);
					v.dist = alternateDist;
					v.previous = u;
					q.add(v);
				}
			}
		}
	}

	/**
	 * After the calculation of the shortest distances between vertices, this
	 * method can be called to provide the shortest path to the ending point
	 * 
	 * @param endName
	 * 
	 * @return an ordered list containing the shortest path
	 */
	public List<Vertex> getShortestPath(String endName) {
		if (!graph.getEdges().containsKey(endName)) {
			System.err.printf("Graph doesn't contain end vertex \"%s\"\n",
					endName);
			return Collections.emptyList();
		}

		List<Vertex> shortestPath = new ArrayList<Vertex>();
		graph.getEdges().get(endName).addToPath(shortestPath);
		return shortestPath;
	}
}
