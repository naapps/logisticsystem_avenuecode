package com.gabrielamorim.logisticsystem.model.graph;

/**
 * 
 * @author Gabriel Amorim
 * 
 *         This class represents an edge with the vertex {@link Edge#v1} being the starting
 *         point, the vertex {@link Edge#v2} being the ending point while {@link Edge#dist} is the
 *         distance between these vertices..
 *
 */
public class Edge {

	public final String v1, v2;
	public final int dist;

	public Edge(String v1, String v2, int dist) {
		this.v1 = v1;
		this.v2 = v2;
		this.dist = dist;
	}
}
