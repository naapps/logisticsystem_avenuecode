package com.gabrielamorim.logisticsystem.dao;

import java.util.Collections;

import com.gabrielamorim.logisticsystem.model.Map;

public class MapDAO extends GenericDAO {

	public void save(Map map) {
		beginTransaction();
		em.persist(map);
		endTransaction();		
	}
	
	public Map getMap(String name) {			
		try {						
			beginTransaction();		
			Map map = em.createQuery("from Map where id = :name", Map.class)
					.setParameter("name", name)
					.setMaxResults(1)
					.getSingleResult();		
			endTransaction();			
			
			return map;
			
		} catch (Exception e) {
			Map map = new Map();
			map.setName("");
			map.setRoutes(Collections.emptyList());			
			return map;
		}		
	}
}
